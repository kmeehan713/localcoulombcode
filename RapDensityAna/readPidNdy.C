#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <istream>
#include <string>

using namespace std;

vector<std::vector <double> > readPidNdy(TString FILE_NAME, int start=-999, int end=999){

  vector <std::vector <double> > pidat;
	vector <double> xval;
	vector <double> y1val;
	vector <double> y1err;
	vector <double> y2val;
	vector <double> y2err;
 
  ifstream myfile(FILE_NAME);
  if(!myfile) //Always test the file open.
	{
	   cout<<"Error opening output file"<<endl;
	}
	int i=0;
	while(!myfile.eof())//why does end of file repeat last line?
	{
	  i++;
	  double a,b,c,d,e;
		myfile>>a>>b>>c>>d>>e;
    if(i<=start || i>end) continue;	  
		xval.push_back(a);
		y1val.push_back(b);
		y1err.push_back(c);
		y2val.push_back(d);
		y2err.push_back(c);
	}

	vector <double> yTotal;
	vector <double> yTotalErr;

  Double_t total, totalErr;

	for (Int_t i=0;i<xval.size();i++){

	  total = y1val.at(i) + y2val.at(i);	
		totalErr = sqrt(y1err.at(i)*y1err.at(i) + y2err.at(i)*y2err.at(i));
		yTotal.push_back(total);
		yTotalErr.push_back(totalErr);

	}

	pidat.push_back(xval);
	pidat.push_back(yTotal);
	pidat.push_back(yTotalErr);

	return pidat;
}//end of test macro
