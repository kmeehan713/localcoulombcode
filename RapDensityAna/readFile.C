#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <istream>
#include <string>

using namespace std;

vector<std::vector <double> > readFile(const char* FILE_NAME, int start=-999, int end=999){
  vector <std::vector <double> > pimdat;
	vector <double> xval;
	vector <double> yval;
	vector <double> yerr;
 
  ifstream myfile(FILE_NAME);
  if(!myfile) //Always test the file open.
	{
	   cout<<"Error opening output file"<<endl;
	}
	int i=0;
	while(!myfile.eof())//why does end of file repeat last line?
	{
	  i++;
	  double a;
		double b;
		double c;
		myfile>>a>>b>>c;
    if(i<=start || i>end) continue;	  
		xval.push_back(a);
		yval.push_back(b);
		yerr.push_back(c);
	}

	pimdat.push_back(xval);
	pimdat.push_back(yval);
	pimdat.push_back(yerr);

	return pimdat;
}//end of macro
