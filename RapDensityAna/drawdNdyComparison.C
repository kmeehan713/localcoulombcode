#include <iostream>
#include <cmath>

#include "TFile.h"
#include "TString.h"
#include "TF1.h"
#include "TGraphErrors.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TLatex.h"

#include "readPidNdy.C"
#include "readFile.C"

using namespace std;

Bool_t draw = true;

//**********************************************************************
//----------------------------------------------------------------------
//
//  Code to draw dNdy for presentation, and plot it with previous experimental data
//
//  Example of how to execute:
//       .x drawdNdyComparison("sysPim.root",0,-1,"e895_pim_dndy.dat","e802_pim_dndy.dat","e877_pim_dndy.dat")
//
//  Note: pid variable is 0 for pions
//
//  Note: This code needs to be updated before use on protons
//
//----------------------------------------------------------------------
//**********************************************************************

void drawdNdyComparison(TString dNdyFileName, Int_t pid, Int_t charge, TString e895DataFileName, TString e802DataFileName=NULL, TString e877DataFileName=NULL){

	TGraphErrors *dNdyGraph, *reflectedGraph, *combinedGraph;
  TF1 *gausFit;
	TString pcleName;

	if (pid==0 && charge<0) pcleName="PionMinus";
	if (pid==0 && charge>0) pcleName="PionPlus";
	if (pid==2) pcleName="ProtonPlus";

	//Obtain the star fxt dNdyGraph
  TFile *dNdyFile = new TFile(dNdyFileName,"READ");	
	dNdyFile->cd();
	dNdyFile->cd(Form("RapidityDensity_%s",pcleName.Data()));
	dNdyGraph = (TGraphErrors *)gDirectory->Get(Form("RapidityDensity_%s_Cent00",pcleName.Data()));
	dNdyFile->cd();
	dNdyGraph->SetMarkerStyle(8);

	//Create the reflected and combined graphs
  Double_t *xarr = dNdyGraph->GetX();
  Double_t *yarr = dNdyGraph->GetY();
  Int_t n = dNdyGraph->GetN();
	const Int_t nPoints = n; //Workaround for err: "non-static-const int for array dimension"
	Double_t xRef[nPoints], yRef[nPoints], xCombined[2*nPoints], yCombined[2*nPoints], yStatErr[2*nPoints];
	Double_t yRef[nPoints], xErr[2*nPoints]; 
	Double_t dNdySysErr[nPoints], totalErr[nPoints];

	for (Int_t i=0;i<nPoints;i++){

		//Reflected points
		xRef[i] = -1*xarr[i];
		yRef[i] = yarr[i];

		//Combined points (reflected + measured)
		xCombined[i] = xarr[i];
		xCombined[nPoints+i] = xRef[i];
		yCombined[i] = yarr[i];
		yCombined[nPoints+i] = yRef[i];

		//Statistical error
		yStatErr[i] = dNdyGraph->GetEY()[i];
		yStatErr[nPoints+i] = dNdyGraph->GetEY()[i];
		xErr[i] = 0.02; //arbitrary to give the error bands width
		xErr[i+nPoints] = 0.02;

		//Estimated systematic error- based on Bose-Einstein and Double-Exponential fits
		if (pid == 0 && charge == -1) dNdySysErr[i] = 0.13*yarr[i];
		if (pid == 0 && charge == 1) dNdySysErr[i] = 0.15*yarr[i];

		//Total error
		totalErr[i] = sqrt(yStatErr[i]*yStatErr[i] + dNdySysErr[i]*dNdySysErr[i]);
		dNdyGraph->SetPointError(i, xErr[i], totalErr[i]);

	}

  reflectedGraph = new TGraphErrors(nPoints,xRef,yRef,0,yStatErr);
	reflectedGraph->SetMarkerStyle(4);
	reflectedGraph->SetMarkerSize(1.5);
  combinedGraph = new TGraphErrors(2*nPoints,xCombined,yCombined,0,yStatErr);
	combinedGraph->SetMarkerStyle(8);
	combinedGraph->SetMarkerColor(0);

	//Create gaussian fit function
  gausFit = new TF1("gaussianFit","gaus(0)",-2,2);
	gausFit->SetParameter(0,70);
	gausFit->SetParameter(1,0);
	gausFit->SetParameter(2,0.5);
	if (pid==0 && charge<0) gausFit->SetLineColor(kRed);
	if (pid==0 && charge>0) gausFit->SetLineColor(kBlue);
	if (pid==2) gausFit->SetLineColor(kGreen);

	//Fit with function: Only fit to measured data range
	if (pid==0 && charge==-1) combinedGraph->Fit(gausFit,"EXO","",-0.33,1.35);
	if (pid==0 && charge==1) combinedGraph->Fit(gausFit,"EXO","",-0.13,1.35);
	
	//Read in AGS data
	//For pions
  if (pid==0) {

		vector<std::vector <double> >  pcleData = readPidNdy(e895DataFileName);
		if (e802DataFileName) vector<std::vector <double> >  e802Data = readFile(e802DataFileName);
		if (e877DataFileName) vector<std::vector <double> >  e877Data = readFile(e877DataFileName);

	}

	//For protons
  if (pid==2) vector<std::vector <double> >  pcleData = readFile(e895DataFileName);
	vector <double> xVal = pcleData.at(0);
	vector <double> yVal = pcleData.at(1);
	vector <double> yErr = pcleData.at(2);

	//Make AGS Graphs
	Int_t nPoints895 = xVal.size();
	TGraphErrors *gE895 = new TGraphErrors();
	for (Int_t iPoint=0; iPoint<nPoints895; iPoint++){

		gE895->SetPoint(iPoint,xVal.at(iPoint),yVal.at(iPoint));
		gE895->SetPointError(iPoint,0,yErr.at(iPoint));

  }	
	cout<<"Created E895 graph"<<endl;
	gE895->SetMarkerStyle(22);
	gE895->SetMarkerColor(kGreen-2);
	gE895->SetMarkerSize(1.5);

	if (e802DataFileName){
		
		vector <double> xVal802 = e802Data.at(0);
		vector <double> yVal802 = e802Data.at(1);
		vector <double> yErr802 = e802Data.at(2);
		Int_t nPoints802 = xVal802.size();
		TGraphErrors *gE802 = new TGraphErrors();
		TGraphErrors *gE802Ref = new TGraphErrors();
		for (Int_t iPoint=0; iPoint<nPoints802; iPoint++){

			gE802->SetPoint(iPoint,xVal802.at(iPoint),yVal802.at(iPoint));
			gE802->SetPointError(iPoint,0,yErr802.at(iPoint));
			gE802Ref->SetPoint(iPoint,-1*xVal802.at(iPoint),yVal802.at(iPoint));
			gE802Ref->SetPointError(iPoint,0,yErr802.at(iPoint));

		}	
		cout<<"Created E802 graph"<<endl;
		gE802->SetMarkerStyle(33);
		gE802->SetMarkerColor(1);
		gE802->SetMarkerSize(2);
		gE802Ref->SetMarkerStyle(27);
		gE802Ref->SetMarkerColor(1);
		gE802Ref->SetMarkerSize(2);

  }
	
	if (e877DataFileName){
		
		vector <double> xVal877 = e877Data.at(0);
		vector <double> yVal877 = e877Data.at(1);
		vector <double> yErr877 = e877Data.at(2);
		Int_t nPoints877 = xVal877.size();
		TGraphErrors *gE877 = new TGraphErrors();
		TGraphErrors *gE877Ref = new TGraphErrors();
		for (Int_t iPoint=0; iPoint<nPoints877; iPoint++){

			gE877->SetPoint(iPoint,xVal877.at(iPoint),yVal877.at(iPoint));
			gE877->SetPointError(iPoint,0,yErr877.at(iPoint));
			gE877Ref->SetPoint(iPoint,-1*xVal877.at(iPoint),yVal877.at(iPoint));
			gE877Ref->SetPointError(iPoint,0,yErr877.at(iPoint));

		}	
		cout<<"Created E877 graph"<<endl;
		gE877->SetMarkerStyle(21);
		gE877->SetMarkerColor(12);
		gE877Ref->SetMarkerStyle(25);
		gE877Ref->SetMarkerColor(12);

  }

	if (draw){

		//Pi Minus are red
		if (pid==0 && charge<0){
			dNdyGraph->SetMarkerColor(kRed);
			reflectedGraph->SetMarkerColor(kRed);
    }
		//Pi Plus are blue
		if (pid==0 && charge>0){
		  dNdyGraph->SetMarkerColor(kBlue);
		  reflectedGraph->SetMarkerColor(kBlue);
		}
		//Protons are green
		if (pid==2){
		  dNdyGraph->SetMarkerColor(kGreen+3);
		  reflectedGraph->SetMarkerColor(kGreen+3);
    }

		TCanvas *c1 = new TCanvas("c1","c1",800,800);
		TH1F *frame   = c1->DrawFrame(-2,0,2,90);
		if (pid==0 && charge ==1) frame->SetTitle("#pi^{+} Rapidity Density");
		if (pid==0 && charge ==-1) frame->SetTitle("#pi^{-} Rapidity Density");
		frame->GetXaxis()->SetTitle("y-y_{CM}");
		frame->GetXaxis()->SetTitleSize(0.04);
		frame->GetYaxis()->SetTitle("dN/dy");
		frame->GetYaxis()->SetTitleSize(0.04);
		frame->GetYaxis()->SetTitleOffset(1.2);
		//Plot statistical error and extrapolated fit function
		combinedGraph->Draw("PZ");
		//Plot measured points with systematic error
		dNdyGraph->SetMarkerSize(1.5);
		if ( pid==0 && charge==-1) dNdyGraph->SetFillColor(46);
		if ( pid==0 && charge==1) dNdyGraph->SetFillColor(9);
		if ( pid==2 && charge==1) dNdyGraph->SetFillColor(8);
		dNdyGraph->SetFillStyle(3001);
		dNdyGraph->Draw("PZ2");
		
		//Plot reflected points with statistical error only
		reflectedGraph->Draw("PZ");
		//combinedGraph->Draw("PZ");
		
		//Plot fit to measured points 
		gausFit->SetLineStyle(7);
		gausFit->Draw("same");

		//Plot previous experiment data
		gE895->Draw("PZ");
	  if (e802DataFileName){
			gE802->Draw("PZ");
			gE802Ref->Draw("PZ");
		}
	  if (e877DataFileName){
			gE877->Draw("PZ");
			gE877Ref->Draw("PZ");
		}
		
		//Create marker symbols for more readable legend
		TMarker *starFXT = new TMarker(0,0,8);
		if (pid==0 && charge==1) starFXT->SetMarkerColor(kBlue);
		if (pid==0 && charge==-1) starFXT->SetMarkerColor(kRed);
		starFXT->SetMarkerSize(1.5);
		TMarker *e895Marker = new TMarker(0,0,22);
		e895Marker->SetMarkerColor(kGreen-2);
		e895Marker->SetMarkerSize(1.5);
	  if (e802DataFileName){
			TMarker *e802Marker = new TMarker(0,0,33);
			e802Marker->SetMarkerColor(1);
			e802Marker->SetMarkerSize(2);
		}
	  if (e877DataFileName){
			TMarker *e877Marker = new TMarker(0,0,21);
			e877Marker->SetMarkerColor(12);
			e877Marker->SetMarkerSize(1.5);
		}

    //Create legend
		TLegend *leg1 = new TLegend(0.13,0.59,0.33,0.89);
		leg1->SetFillColor(kWhite);
		leg1->SetBorderSize(0);
		leg1->SetTextSize(0.03);
		leg1->AddEntry(starFXT,"STAR FXT 4.5 GeV","P");
		leg1->AddEntry(e895Marker,"E895 4.3 GeV","P");
		if (e802DataFileName) leg1->AddEntry(e802Marker,"E802 4.9 GeV","P");
	  if (e877DataFileName)	leg1->AddEntry(e877Marker,"E877 4.9 GeV","P");
		leg1->Draw("SAME");

		//Create star preliminary text
		TPaveText *starPrelim = new TPaveText(.35,.15,.70,.21,"BRNBNDC");
    starPrelim->SetFillColor(kWhite);
    starPrelim->SetBorderSize(0);
    starPrelim->SetTextFont(65);//63
    starPrelim->SetTextSize(18);
    starPrelim->AddText("STAR PRELIMINARY");
    starPrelim->Draw("SAME");

	}

}// End of fnxn
