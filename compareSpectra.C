#include <vector>

#include "readFile.C"

using namespace std;

void compareSpectra(TString mySpectraFileName, TString otherSpectraFileName, TString outFileName, Int_t charge){

	//Get my midrapidity spectra
	TFile *mySpectraFile = new TFile(mySpectraFileName,"READ");
	TGraphErrors *mySpectra, *otherSpectra;
  mySpectraFile->cd();
  if (charge == -1) 
				mySpectra = (TGraphErrors*)gDirectory->Get("CorrectedSpectra_PionMinus/correctedSpectra_PionMinus_Cent00_yIndex05");
  if (charge == 1) 
				mySpectra = (TGraphErrors*)gDirectory->Get("CorrectedSpectra_PionPlus/correctedSpectra_PionPlus_Cent00_yIndex05");

  //Get other spectra data
  vector<std::vector <double> >	otherData = readFile(otherSpectraFileName);
  vector <double> xVal = otherData.at(0);
  vector <double> yVal = otherData.at(1);
  vector <double> yErr = otherData.at(2);
  Int_t nPoints = xVal.size(); 

  otherSpectra = new TGraphErrors();

  for (Int_t iPoint; iPoint<nPoints; iPoint++){

    otherSpectra->SetPoint(iPoint,xVal.at(iPoint),yVal.at(iPoint));
    otherSpectra->SetPointError(iPoint,0,yErr.at(iPoint));

  }

	//Draw both spectra for comparison
  TCanvas *c1 = new TCanvas("c1","c1",600,600);
	TH1F *frame = c1->DrawFrame(0,0.01,1.5,500);
	if (charge == -1) mySpectra->SetMarkerColor(2);
	if (charge == 1) mySpectra->SetMarkerColor(kBlue);
	otherSpectra->SetMarkerStyle(22);
  mySpectra->Draw("PZ");
  otherSpectra->Draw("PZ");
  
	//Save Jenn Klay's spectra for further use
	TFile *outFile = new TFile(outFileName, "UPDATE");
  if (charge == -1) otherSpectra->SetName("preparedSpectra_PionMinus_Cent00_yIndex05");
  if (charge == 1) otherSpectra->SetName("preparedSpectra_PionPlus_Cent00_yIndex05");
  otherSpectra->Write();
  	
}//END MACRO
